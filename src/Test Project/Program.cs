﻿using System;
using Tao.FreeGlut;
using OpenGL;

namespace Test_Project
{
    class Program
    {
        private static int ViewportWidth = 800, ViewportHeight = 600;
        private static ShaderProgram shadyProgram;
        private static VBO<Vector3> triangle, square;
        private static VBO<int> triangleElements, squareElements;

        static void Main(string[] args)
        {
            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);
            Glut.glutInitWindowSize(ViewportWidth, ViewportHeight);
            Glut.glutCreateWindow("Dead Give Away");

            Glut.glutIdleFunc(OnRenderFrame);
            Glut.glutDisplayFunc(OnDisplay);

            shadyProgram = new ShaderProgram(VertexShader, FragmentShader);

            shadyProgram.Use();
            shadyProgram["projection_matrix"].SetValue(Matrix4.CreatePerspectiveFieldOfView(0.45f, (float)(ViewportWidth / ViewportHeight), 0.1f, 1000f));
            shadyProgram["view_matrix"].SetValue(Matrix4.LookAt(new Vector3(0, 0, 20), Vector3.Zero, Vector3.Up));

            triangle = new VBO<Vector3>(new Vector3[] { new Vector3(0, 0, 0), new Vector3(0, -1, 0), new Vector3(-1, 0, 0) });
            square = new VBO<Vector3>(new Vector3[] { new Vector3(-1, 1, 0), new Vector3(1, 1, 0), new Vector3(1, -1, 0), new Vector3(-1, -1, 0) });

            triangleElements = new VBO<int>(new int[] { 0, 1, 2 }, BufferTarget.ElementArrayBuffer);
            squareElements = new VBO<int>(new int[] { 0, 1, 2, 3 }, BufferTarget.ElementArrayBuffer);

            Glut.glutMainLoop();
        }
        private static void OnDisplay()
        {

        }
        private static void OnRenderFrame()
        {
            // Clear Prev Frame
            Gl.Viewport(0, 0, ViewportWidth, ViewportHeight);
            Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            shadyProgram.Use();

            // draw triangle
            shadyProgram["model_matrix"].SetValue(Matrix4.CreateTranslation(new Vector3(-1.5f, 0, 0)));
            uint vertexPositionIndex = (uint)Gl.GetAttribLocation(shadyProgram.ProgramID, "vertexPosition");
            Gl.EnableVertexAttribArray(vertexPositionIndex);
            Gl.BindBuffer(triangle);
            Gl.VertexAttribPointer(vertexPositionIndex, triangle.Size, triangle.PointerType, true, 12, IntPtr.Zero);
            Gl.BindBuffer(triangleElements);
            Gl.DrawElements(BeginMode.Triangles, triangleElements.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);

            // Draw square
            shadyProgram["model_matrix"].SetValue(Matrix4.CreateTranslation(new Vector3(1.5f, 0, 0)));
            Gl.BindBufferToShaderAttribute(square,shadyProgram,"vertexPosition");
            Gl.BindBuffer(squareElements);
            Gl.DrawElements(BeginMode.Quads, squareElements.Count, DrawElementsType.UnsignedInt, IntPtr.Zero);

            Glut.glutSwapBuffers();
        }

        // I hate writing code within a string!
        public static string VertexShader = @"
in vec3 vertexPosition;
uniform mat4 projection_matrix;
uniform mat4 view_matrix;
uniform mat4 model_matrix;

void main(void)
{
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vertexPosition, 1);
}
";
        public static string FragmentShader = @"
void main(void)
{
    gl_FragColor = vec4(1, 1, 1, 1);
}
";
    }
}
